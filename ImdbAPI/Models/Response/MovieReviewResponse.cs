﻿using System.Collections.Generic;

namespace ImdbAPI.Models.Response
{
    public class MovieReviewResponse
    {
        public IEnumerable<Review> Comments { get; set; }
    }
}
