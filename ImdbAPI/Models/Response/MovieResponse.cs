﻿using System.Collections.Generic;


namespace ImdbAPI.Models.Response
{
    public class MovieResponse
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string YearOfRelease { get; set; }

        public string Plot { get; set; }

        public string Poster { get; set; }

        public IEnumerable<ActorResponse> Actors { get; set; }

        public IEnumerable<GenreResponse> Genres { get; set; }

        public ProducerResponse Producer { get; set; }
    }
}
