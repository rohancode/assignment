﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ImdbAPI.Models
{
    public class Person
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [StringLength(150)]
        public string Bio { get; set; }

        [Required]
        public DateTime DOB { get; set; }

        public string Gender { get; set; }
    }
}
