﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImdbAPI.Models.Request
{
    public class GenreRequest
    {
        public string Name { get; set; }
    }
}
