﻿namespace ImdbAPI.Models.Request
{
    public class MovieReviewRequest
    {
        public string  Comment { get; set; }
    }
}
