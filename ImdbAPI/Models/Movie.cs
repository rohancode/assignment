﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ImdbAPI.Models
{
    public class Movie
    {
        public int Id { get; set; }

        [Required,StringLength(50)]
        public string Name { get; set; }

        public DateTime YearOfRelease { get; set; }

        [Required,StringLength(150)]
        public string Plot { get; set; }

        public string Poster { get; set; }

        [Required]
        public int ProducerId { get; set; }

    }
}
