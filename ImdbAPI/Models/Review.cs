﻿namespace ImdbAPI.Models
{
    public class Review
    {
        public int Id { get; set; }
        public string Comment { get; set; }
    }
}
