﻿using Microsoft.AspNetCore.Mvc;
using ImdbAPI.Services.Interface;
using ImdbAPI.Models.Request;
using Firebase.Storage;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;
using System;

namespace ImdbAPI.Controllers
{
    [ApiController]
    [Route("movies")]
    public class MovieController : Controller
    {
        private readonly IMovieService _movieService;
        public MovieController(IMovieService movieService)
        {
            _movieService = movieService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {

            var movies = _movieService.GetAll();
            return Ok(movies);

        }

        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {

            var movie = _movieService.GetById(id);
            return Ok(movie);
        }

        [HttpPost]
        public IActionResult Post([FromBody] MovieRequest movie)
        {
            _movieService.Add(movie);
            return Ok(movie);
        }

        
        [HttpPost("upload")]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            if (file == null || file.Length == 0)
                return Content("file not selected");
            var task = await new FirebaseStorage("imdbapi-b2ba0.appspot.com")
                    .Child("images")
                    .Child(Guid.NewGuid().ToString() + ".jpg")
                    .PutAsync(file.OpenReadStream());
            return Ok(task);
        }


        [HttpDelete("{id}")]
        public  IActionResult Delete(int id)
        {
            _movieService.Delete(id);
            return Ok("Deleted success!");
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] MovieRequest movie)
        {

            _movieService.Update(id, movie);
            return Ok(movie);
        }





    }
}
