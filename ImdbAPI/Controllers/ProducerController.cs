﻿using Microsoft.AspNetCore.Mvc;
using ImdbAPI.Models.Request;
using ImdbAPI.Services.Interface;

namespace ImdbAPI.Controllers
{
    [ApiController]
    [Route("producers")]
    public class ProducerController : Controller
    {
        private readonly IProducerService _producerService;
        public ProducerController(IProducerService producerService)
        {
            _producerService = producerService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {

            var producers = _producerService.GetAll();
            return Ok(producers);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {

            var producer = _producerService.GetById(id);
            return Ok(producer);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ProducerRequest producer)
        {

            var id = _producerService.Add(producer);
            return Ok(id);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ProducerRequest producer)
        {

            _producerService.Update(id, producer);
            return Ok(producer);

        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

            _producerService.Delete(id);
            return Ok("Delete success!");

        }

    }
}

