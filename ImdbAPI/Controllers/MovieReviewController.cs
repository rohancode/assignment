﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ImdbAPI.Services.Interface;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ImdbAPI.Models.Request;


namespace ImdbAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Route("movie")]
    public class MovieReviewController : ControllerBase
    {
        private readonly IMovieReviewService _movieReviewService;
        public MovieReviewController(IMovieReviewService movieReviewService)
        {
            _movieReviewService = movieReviewService;
        }

        [HttpGet("{id}/review")]
        public IActionResult Get(int id)
        {
           var reviews =  _movieReviewService.Get(id);
           return Ok(reviews);

        }

        [HttpPost("{id}/review")]
        public IActionResult Post(int id,[FromBody]MovieReviewRequest reviewRequest)
        {
            _movieReviewService.Add(id,reviewRequest);
            return Ok(reviewRequest);
        }

        [HttpPut("{movieid}/review/{commentid}")]
        public IActionResult Put(int commentid,[FromBody]MovieReviewRequest reviewRequest)
        {
            _movieReviewService.Update(commentid, reviewRequest);
            return Ok();
        }
        [HttpDelete("{movieid}/review/{commentid}")]
        public IActionResult Delete(int commentid)
        {
            _movieReviewService.Delete(commentid);
            return Ok();
        }
    }
}
