﻿using Microsoft.AspNetCore.Mvc;
using ImdbAPI.Services.Interface;
using ImdbAPI.Models.Request;
using System.Collections.Generic;

namespace ImdbAPI.Controllers
{
    [ApiController]
    [Route("actors")]
    public class ActorController : Controller
    {
        private readonly IActorService _actorService;
        public ActorController(IActorService actorService)
        {
            _actorService = actorService;
        }

        [HttpGet]
        public IActionResult GetAll()
        {

            var actors = _actorService.GetAll();
            return Ok(actors);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {

            var actor = _actorService.GetById(id);
            return Ok(actor);
        }

        [HttpPost]
        public IActionResult Post([FromBody] ActorRequest actor)
        {

            var id = _actorService.Add(actor);
            return Ok(id);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] ActorRequest actor)
        {

            _actorService.Update(id, actor);
            return Ok(actor);
        }

        [HttpPatch("{id}")]
        public IActionResult Patch(int id, [FromBody] Dictionary<string, string> patchActor)
        {
            _actorService.PartialUpdate(id, patchActor);
            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _actorService.Delete(id);
            return Ok("Delete success!");
        }

    }
}
