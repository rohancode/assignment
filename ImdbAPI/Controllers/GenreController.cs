﻿using Microsoft.AspNetCore.Mvc;
using ImdbAPI.Services.Interface;
using ImdbAPI.Models.Request;

namespace ImdbAPI.Controllers
{
    [ApiController]
    [Route("genres")]
    public class GenreController : Controller
    {
        private readonly IGenreService _genreService;
        public GenreController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        [HttpGet]
        public IActionResult GetALl()
        {
            
                var genres = _genreService.GetAll();
                return Ok(genres);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int Id)
        {
           
                var genre = _genreService.GetById(Id);
                return Ok(genre);
        }

        [HttpPost]
        public IActionResult Post([FromBody] GenreRequest genre)
        {
                var id = _genreService.Add(genre);
                return Ok(id);
        }

        [HttpPut("{id}")]
        public IActionResult Put(int id, GenreRequest genre)
        {

                _genreService.Update(id, genre);
                return Ok(genre);
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {

                _genreService.Delete(id);
                return Ok("Deleted Success!");
        }

    }
}
