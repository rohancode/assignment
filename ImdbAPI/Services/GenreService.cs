﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbAPI.Services.Interface;
using ImdbAPI.Repository;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Models;

namespace ImdbAPI.Services
{
    public class GenreService : IGenreService
    {
        private readonly IGenreRepository _genreRepository;
        public GenreService(IGenreRepository genreRepository)
        {
            _genreRepository = genreRepository;
        }
        public int Add(GenreRequest genreRequest)
        {
            if(string.IsNullOrEmpty(genreRequest.Name))
            {
                throw new ArgumentNullException("Can not be null!");
            }

            var genre = new Genre() { Name = genreRequest.Name };

            int id = _genreRepository.Add(genre);
            return id;

        }

        public void Delete(int Id)
        {
            _genreRepository.Delete(Id);
        }

        public IEnumerable<GenreResponse> GetAll()
        {
            var geners = _genreRepository.GetAll().Select(genre => new GenreResponse()
            {
                Id = genre.Id,
                Name = genre.Name
            });

            return geners;
        }

        public GenreResponse GetById(int id)
        {
            var gener = _genreRepository.GetById(id);
            return new GenreResponse() 
                        { Id = gener.Id,
                          Name = gener.Name 
                        };

        }

        public void Update(int id, GenreRequest genreRequest)
        {
            var genre = new Genre()
            {
                Name = genreRequest.Name,
            };
            _genreRepository.Update(id, genre);
        }
    }
}
