﻿using ImdbAPI.Models.Request;
using ImdbAPI.Services.Interface;
using ImdbAPI.Repository.Interface;
using ImdbAPI.Models;
using ImdbAPI.Models.Response;

namespace ImdbAPI.Services
{
    public class MovieReviewService : IMovieReviewService
    {
        private readonly IMovieReviewRepository _movieReviewRepository;

        public MovieReviewService(IMovieReviewRepository movieReviewRepository)
        {
            _movieReviewRepository = movieReviewRepository;
        }

        public void Add(int movieId, MovieReviewRequest reviewRequest)
        {
            _movieReviewRepository.Add(movieId,
                                        new Review()
                                        {
                                            Comment = reviewRequest.Comment
                                        }
                                      );
        }

        public void Delete(int id)
        {
            _movieReviewRepository.Delete(id);
        }

        public MovieReviewResponse Get(int movieId)
        {
            var reviews = _movieReviewRepository.Get(movieId);
            return new MovieReviewResponse()
            {
                Comments = reviews
            };
        }

        public void Update(int id, MovieReviewRequest reviewRequest)
        {
            _movieReviewRepository.Update(id,
                                        new Review()
                                        {
                                            Comment = reviewRequest.Comment
                                        });
        }
    }
}
