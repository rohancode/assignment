﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbAPI.Models;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Services.Interface;
using ImdbAPI.Repository.Interface;
using ImdbAPI.Repository;

namespace ImdbAPI.Services
{
    public class MovieService : IMovieService
    {
        private readonly IMovieRepository _movieRepository;
        private readonly IActorRepository _actorRepository;
        private readonly IGenreRepository _genreRepository;
        private readonly IProducerRepository _producerRepository;

        public MovieService(IMovieRepository movieRepository, IActorRepository actorRepository, IGenreRepository genreRepository, IProducerRepository producerRepository)
        {
            _movieRepository = movieRepository;
            _actorRepository = actorRepository;
            _genreRepository = genreRepository;
            _producerRepository = producerRepository;
        }

        public void Add(MovieRequest movieRequest)
        {
            var movie = new Movie()
            {
                Name = movieRequest.Name,
                Plot = movieRequest.Plot,
                YearOfRelease = DateTime.ParseExact(movieRequest.YearOfRelease, "yyyy/MM/dd", null),
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster
            };
            _movieRepository.Add(movie, movieRequest.ActorsId, movieRequest.GenresId);
        }

        public void Delete(int id)
        {
            if(id<=0)
            {
                throw new Exception("not valid!");
            }
            _movieRepository.Delete(id);
        }

        public IEnumerable<MovieResponse> GetAll()
        { 
            IEnumerable<MovieResponse> movies = _movieRepository.GetAll().Select(movie =>
            {
                Producer producer = _producerRepository.GetById(movie.ProducerId);
                ProducerResponse producerResponse = new ProducerResponse()
                {
                    Id = producer.Id,
                    Name = producer.Name,
                    Gender = producer.Gender,
                    DOB = producer.DOB.ToString("dd/MM/yyyy"),
                    Bio = producer.Bio
                };
                var movieRespone = new MovieResponse()
                {
                    Id = movie.Id,
                    Name = movie.Name,
                    YearOfRelease = movie.YearOfRelease.ToString("yyyy/MM/dd"),
                    Plot = movie.Plot,
                    Poster = movie.Poster,
                    Producer = producerResponse,
                    Actors = _actorRepository.GetByMovieId(movie.Id).Select(actor => new ActorResponse()
                    {
                        Id = actor.Id,
                        Name = actor.Name,
                        Bio = actor.Bio,
                        DOB = actor.DOB.ToString("dd/MM/yyyy"),
                        Gender = actor.Gender
                    }),
                    Genres = _genreRepository.GetByMovieId(movie.Id).Select(gener => new GenreResponse()
                    {
                        Id = gener.Id,
                        Name = gener.Name
                    })
                };
                return movieRespone;
            });

            return movies;

        }

        public MovieResponse GetById(int id)
        {
            var movie = _movieRepository.GetById(id);
            
            var actors = _actorRepository.GetByMovieId(movie.Id).Select(actor => new ActorResponse()
            {
                Id = actor.Id,
                Name = actor.Name,
                Bio = actor.Bio,
                DOB = actor.DOB.ToString("dd/MM/yyyy"),
                Gender = actor.Gender
            });

            var geners = _genreRepository.GetByMovieId(movie.Id).Select(genre => new GenreResponse()
            {
                Id = genre.Id,
                Name = genre.Name
            });

            var producer = _producerRepository.GetById(movie.ProducerId);
            var producerResponse = new ProducerResponse
            {
                Id = producer.Id,
                Name = producer.Name,
                DOB = producer.DOB.ToString("dd/MM/yyyy"),
                Gender = producer.Gender,
                Bio = producer.Bio
            };


            return new MovieResponse()
            {
                Id = movie.Id,
                Name = movie.Name,
                YearOfRelease = movie.YearOfRelease.ToString("yyyy/MM/dd"),
                Plot = movie.Plot,
                Poster = movie.Poster,
                Producer = producerResponse,
                Actors = actors,
                Genres = geners
            };
        }

        public void Update(int id, MovieRequest movieRequest)
        {
            if(id<=0)
            {
                throw new IndexOutOfRangeException("Not valid Id");
            }
            var movie = new Movie()
            {
                Name = movieRequest.Name,
                Plot = movieRequest.Plot,
                YearOfRelease = DateTime.ParseExact(movieRequest.YearOfRelease,"yyyy/MM/dd",null),
                ProducerId = movieRequest.ProducerId,
                Poster = movieRequest.Poster
            };
            _movieRepository.Update(id, movie, movieRequest.ActorsId, movieRequest.GenresId);

        }
    }
}
