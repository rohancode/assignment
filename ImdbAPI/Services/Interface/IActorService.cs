﻿using System.Collections.Generic;
using ImdbAPI.Models.Response;
using ImdbAPI.Models.Request;


namespace ImdbAPI.Services.Interface
{
    public interface IActorService
    {
        IEnumerable<ActorResponse> GetAll();
        ActorResponse GetById(int id);
        void Update(int id, ActorRequest actorRequest);
        void PartialUpdate(int id, Dictionary<string, string> patchActor);
        void Delete(int id);
        int Add(ActorRequest actorRequest);
    }
}
