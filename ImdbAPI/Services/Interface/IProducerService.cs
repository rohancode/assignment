﻿using System.Collections.Generic;
using ImdbAPI.Models.Response;
using ImdbAPI.Models.Request;

namespace ImdbAPI.Services.Interface
{
    public interface IProducerService
    {
        IEnumerable<ProducerResponse> GetAll();
        ProducerResponse GetById(int id);
        void Update(int id, ProducerRequest producerRequest);
        void Delete(int id);
        int Add(ProducerRequest producerRequest);
    }
}
