﻿using System.Collections.Generic;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;

namespace ImdbAPI.Services.Interface
{
    public interface IGenreService
    {
        IEnumerable<GenreResponse> GetAll();
        GenreResponse GetById(int id);
        void Update(int id, GenreRequest genreRequest);
        void Delete(int id);
        int Add(GenreRequest genreRequest);
    }
}
