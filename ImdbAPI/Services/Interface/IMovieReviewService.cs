﻿using ImdbAPI.Models.Response;
using ImdbAPI.Models.Request;

namespace ImdbAPI.Services.Interface
{
    public interface IMovieReviewService
    {
        MovieReviewResponse Get(int movieId);
        void Add(int movidId, MovieReviewRequest reviewRequest);
        void Update(int id, MovieReviewRequest reviewRequest);
        void Delete(int id);
    }
}
