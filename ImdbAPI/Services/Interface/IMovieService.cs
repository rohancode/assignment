﻿using System.Collections.Generic;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;

namespace ImdbAPI.Services.Interface
{
    public interface IMovieService
    {
        IEnumerable<MovieResponse> GetAll();
        MovieResponse GetById(int id);
        void Update(int id, MovieRequest movieRequest);
        void Delete(int id);
        void Add(MovieRequest movieRequest);
    }
}
