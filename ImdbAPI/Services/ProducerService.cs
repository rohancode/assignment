﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbAPI.Models.Request;
using ImdbAPI.Models.Response;
using ImdbAPI.Services.Interface;
using ImdbAPI.Repository;
using ImdbAPI.Models;

namespace ImdbAPI.Services
{
    public class ProducerService : IProducerService
    {
        private readonly IProducerRepository _producerRepository;
        public ProducerService(IProducerRepository producerRepository)
        {
            _producerRepository = producerRepository;
        }
        public IEnumerable<ProducerResponse> GetAll()
        {
            return _producerRepository.GetAll().Select(
                                    producer =>
                                    new ProducerResponse()
                                    {
                                        Name = producer.Name,
                                        Bio = producer.Bio,
                                        DOB = producer.DOB.ToString("yyyy/MM/dd"),
                                        Id = producer.Id,
                                        Gender = producer.Gender
                                    }
                                    );
        }
        public ProducerResponse GetById(int id)
        {
            var producer = _producerRepository.GetById(id);
            return new ProducerResponse()
            {
                Id = producer.Id,
                Name = producer.Name,
                Bio = producer.Bio,
                DOB = producer.DOB.ToString("yyyy/MM/dd"),
                Gender = producer.Gender
            };
        }
        public int Add(ProducerRequest producerRequest)
        {
            string name = producerRequest.Name;
            string gender = producerRequest.Gender;
            string dob = producerRequest.DOB;
            string bio = producerRequest.Bio;

            if (string.IsNullOrEmpty(name) ||
               string.IsNullOrEmpty(gender) ||
               string.IsNullOrEmpty(dob)
              )
            {
                throw new ArgumentNullException("This can not be empty!");
            }

            var producer = new Producer()
            {
                Name = name,
                Gender = gender,
                DOB = DateTime.ParseExact(dob, "yyyy/MM/dd", null),
                Bio = bio,
            };

            var id = _producerRepository.Add(producer);
            return id;
        }

        public void Delete(int Id)
        {
            _producerRepository.Delete(Id);
        }
        public void Update(int id, ProducerRequest producerRequest)
        {
            var producer = new Producer()
            {
                Name = producerRequest.Name,
                Gender = producerRequest.Gender,
                DOB = DateTime.ParseExact(producerRequest.DOB, "yyyy/MM/dd", null),
                Bio = producerRequest.Bio,
            };
            _producerRepository.Update(id, producer);
        }
    }
}
