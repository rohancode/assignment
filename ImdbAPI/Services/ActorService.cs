﻿using System;
using System.Collections.Generic;
using System.Linq;
using ImdbAPI.Models.Response;
using ImdbAPI.Models.Request;
using ImdbAPI.Repository;
using ImdbAPI.Models;
using ImdbAPI.Services.Interface;


namespace ImdbAPI.Services
{
    public class ActorService : IActorService
    {
        
        private readonly IActorRepository _actorRepository;
        public ActorService(IActorRepository actorRepository)
        {
            _actorRepository = actorRepository;
        }
        public IEnumerable<ActorResponse> GetAll()
        {
            return _actorRepository.GetAll().Select(
                                                actor =>
                                                new ActorResponse()
                                                {
                                                    Name = actor.Name,
                                                    Bio = actor.Bio,
                                                    DOB = actor.DOB.ToString("yyyy/MM/dd"),
                                                    Id = actor.Id,
                                                    Gender = actor.Gender
                                                }
                                                );
        }
        public ActorResponse GetById(int id)
        {
            var actor = _actorRepository.GetById(id);
            return new ActorResponse() 
                                    { 
                                    Id=actor.Id, 
                                    Name=actor.Name,
                                    Bio = actor.Bio,
                                    DOB = actor.DOB.ToString("yyyy/MM/dd"),
                                    Gender = actor.Gender
                                    };

        }
        public void Update(int id,ActorRequest actorRequest)
        {
            
            var actor = new Actor()
                                       {
                                        Name = actorRequest.Name,
                                        Gender = actorRequest.Gender,
                                        DOB = DateTime.ParseExact(actorRequest.DOB,"yyyy/MM/dd",null),
                                        Bio = actorRequest.Bio,
                                       };
            _actorRepository.Update(id, actor);

        }

        public void PartialUpdate(int id,Dictionary<string,string> patchActor)
        {
            _actorRepository.PartialUpdate(id, patchActor);
        }

        public void Delete(int Id)
        {
            _actorRepository.Delete(Id);
        }
        public int Add(ActorRequest actorRequest)
        {
            string name = actorRequest.Name;
            string gender = actorRequest.Gender;
            string dob = actorRequest.DOB;
            string bio = actorRequest.Bio;
            
            if(string.IsNullOrEmpty(name) ||
               string.IsNullOrEmpty(gender) ||
               string.IsNullOrEmpty(dob)
              )
            {
                throw new ArgumentNullException("This can not be empty!");
            }

            var actor = new Actor()
                            {
                                Name = name,
                                Gender = gender,
                                DOB = DateTime.ParseExact(dob, "yyyy/MM/dd", null),
                                Bio = bio,
                            };

            var id = _actorRepository.Add(actor);
            return id;
        }


    }
}
