﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImdbAPI.Models;

namespace ImdbAPI.Repository.Interface
{
    public interface IMovieReviewRepository
    {
        public void Add(int movieId,Review review);
        public void Delete(int id);
        public void Update(int id,Review review);
        public IEnumerable<Review> Get(int movieId);
    }
}
