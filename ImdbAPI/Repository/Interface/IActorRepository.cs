﻿using System.Collections.Generic;
using ImdbAPI.Models;

namespace ImdbAPI.Repository
{
    public interface IActorRepository
    {
        IEnumerable<Actor> GetAll();
        Actor GetById(int id);
        IEnumerable<Actor> GetByMovieId(int Id);
        int Add(Actor actor);
        void Update(int id, Actor actor);
        void PartialUpdate(int id, Dictionary<string, string> patchActor);
        void Delete(int id);
    }
}
