﻿using System;
using System.Collections.Generic;
using ImdbAPI.Models;


namespace ImdbAPI.Repository
{
    public interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        IEnumerable<Genre> GetByMovieId(int Id);
        int Add(Genre genre);
        void Update(int id, Genre genre);
        void Delete(int id);
    }
}
