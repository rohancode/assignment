﻿using System;
using System.Collections.Generic;
using ImdbAPI.Models;

namespace ImdbAPI.Repository
{
    public interface IProducerRepository
    {
        IEnumerable<Producer> GetAll();
        Producer GetById(int id);
        int Add(Producer producer);
        void Update(int id, Producer producer);
        void Delete(int id);

    }
}
