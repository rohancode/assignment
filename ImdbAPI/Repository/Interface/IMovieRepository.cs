﻿using System.Collections.Generic;
using ImdbAPI.Models;

namespace ImdbAPI.Repository.Interface
{
    public interface IMovieRepository
    {
        IEnumerable<Movie> GetAll();
        Movie GetById(int id);
        void Add(Movie movie, List<int> actorsId, List<int> genresId);
        void Update(int id, Movie movie,List<int>actorsId,List<int>genresId);
        void Delete(int id);
    }
}
