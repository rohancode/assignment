﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ImdbAPI.Models;
using ImdbAPI.Repository.Interface;
using Microsoft.Extensions.Options;

namespace ImdbAPI.Repository
{
    public class MovieRepository : IMovieRepository
    {

        private readonly ConnectionString _connectionString;

        public MovieRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public IEnumerable<Movie> GetAll()
        {
            const string query = @"
SELECT M.Id
	,M.Name
	,M.YearOfRelease
	,M.Plot
	,M.ProducerId
	,M.Poster
FROM Movies M";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var movies = connection.Query<Movie>(query);
            return movies;
        }
        public Movie GetById(int id)
        {
            const string query = @"
SELECT M.Id
	,M.Name
	,M.YearOfRelease
	,M.Plot
	,M.ProducerId
	,M.Poster
FROM Movies M
WHERE M.Id = @Id";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var movie = connection.QueryFirst<Movie>(query, new { Id = id });

            return movie;
        }
        public void Add(Movie movie,List<int>actorsId, List<int>genresId)
        {
            const string sql = @"INSERT_MOVIE";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql,
                 new
                 {
                     movie.Name,
                     movie.YearOfRelease,
                     movie.Plot,
                     movie.ProducerId,
                     movie.Poster,
                     ActorsId = string.Join(',',actorsId),
                     GenresId = string.Join(',',genresId)
                 },
                commandType: CommandType.StoredProcedure);

        }
        public void Delete(int id)
        {
            const string sql = @"DELETE_MOVIE";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { MovieId = id }, commandType: CommandType.StoredProcedure);
        }
        public void Update(int id, Movie movie, List<int>actorsId, List<int>genresId)
        {
            const string sql = @"MOVIE_UPDATE";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql,
                new
                {
                    movie.Name,
                    movie.YearOfRelease,
                    movie.Plot,
                    movie.ProducerId,
                    movie.Poster,
                    ActorsId = string.Join(',',actorsId),
                    GenresId = string.Join(',',genresId),
                    Id = id
                },
                commandType: CommandType.StoredProcedure);

        }
    }
}
