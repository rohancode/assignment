﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ImdbAPI.Models;
using Microsoft.Extensions.Options;

namespace ImdbAPI.Repository
{
    public class ActorRepository : IActorRepository
    {
        private readonly ConnectionString _connectionString;
        public ActorRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public IEnumerable<Actor> GetAll()
        {
            const string query = @"
SELECT ID
	,NAME
	,GENDER
	,DOB
	,BIO
FROM Actors";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var actors = connection.Query<Actor>(query);
            return actors;
        }
        public Actor GetById(int id)
        {
            const string query = @"
SELECT ID
	,NAME
	,GENDER
	,DOB
	,BIO
FROM Actors
WHERE ID = @Id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var actor = connection.QueryFirst<Actor>(query,new { Id = id});
            return actor;
        }

        public IEnumerable<Actor>GetByMovieId(int Id)
        {
            const string query = @"
SELECT A.Id
    ,A.Name
	,A.Bio
	,A.gender
	,A.DOB
FROM Actors A
INNER JOIN MoviesActorMapping MA ON A.Id = MA.ActorId
WHERE MA.MovieId = @Id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var actors = connection.Query<Actor>(query, new { Id = Id });

            return actors;
        }

        public int Add(Actor actor)
        {
            const string sql = @"
INSERT INTO Actors
VALUES (
	@Name
	,@Gender
	,@DOB
	,@BIO
	)
SELECT SCOPE_IDENTITY()";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var id = connection.Query<int>(sql, new
            {
                actor.Name,
                actor.Gender,
                DOB = actor.DOB.ToString("yyyy/MM/dd"),
                actor.Bio
            });

            return id.Single();

        }

        public void Update(int id, Actor actor)
        {
            const string sql = @"
UPDATE ACTORS
SET NAME = @NAME
	,DOB = @DOB
	,BIO = @BIO
	,GENDER = @GENDER
WHERE ID = @ID";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new
            {
                ID=id,
                actor.Name,
                DOB = actor.DOB.ToString("yyyy/MM/dd"),
                actor.Bio,
                actor.Gender
            });
        }

        public void PartialUpdate(int id,Dictionary<string,string> patchActor)
        {
            string sql = @"UPDATE ACTORS SET ";
            foreach(var i in patchActor)
            {
                sql += i.Key + "=" + "'" + i.Value + "'";
            }
            sql += " WHERE Id=@id";
            
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { Id = id });
        }

        public void Delete(int id)
        {
            const string sql = @"
DELETE MoviesActorMapping
FROM MoviesActorMapping MA
WHERE MA.ActorId = @Id

DELETE
FROM Actors
WHERE Actors.Id = @Id;";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { ID = id });

        }
    }
}
