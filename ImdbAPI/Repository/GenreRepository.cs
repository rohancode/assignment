﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using ImdbAPI.Models;
using Microsoft.Extensions.Options;
using System.Linq;

namespace ImdbAPI.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private readonly ConnectionString _connectionString;

        public GenreRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }
        public int Add(Genre genre)
        {
            const string sql = @"
INSERT INTO Genres
VALUES (@name)
SELECT SCOPE_IDENTITY()";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var id = connection.Query<int>(sql, new { name = genre.Name });
            return id.Single();
        }

        public void Delete(int id)
        {
            const string sql = @"
DELETE GenresMoviesMapping
FROM GenresMoviesMapping GM
WHERE GM.GenresId = @Id

DELETE
FROM Genres
WHERE Genres.Id = @Id";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { ID = id });
        }

        public IEnumerable<Genre> GetAll()
        {
            const string query = @"
SELECT Id
     ,Name
FROM Genres";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var genres = connection.Query<Genre>(query);

            return genres;
        }

        public Genre GetById(int id)
        {
            const string query = @"
SELECT Id
	,Name
FROM Genres
WHERE Id = @Id";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var genre = connection.QueryFirst(query);

            return genre;

        }

        public IEnumerable<Genre>GetByMovieId(int Id)
        {
            const string query = @"
SELECT G.Id
      ,G.Name
FROM Genres G
INNER JOIN GenresMoviesMapping GM ON G.Id = GM.GenresId
WHERE GM.MovieId = @Id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var genres = connection.Query<Genre>(query, new { Id = Id });

            return genres;

        }

        public void Update(int id, Genre genre)
        {
            const string sql = @"
UPDATE Genres
SET Name = @Name
WHERE Id = @id";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { genre.Name,Id = id });
        }
    }
}
