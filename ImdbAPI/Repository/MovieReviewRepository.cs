﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper;
using ImdbAPI.Models;
using Microsoft.Extensions.Options;
using ImdbAPI.Repository.Interface;
using System.Data;

namespace ImdbAPI.Repository
{
    public class MovieReviewRepository : IMovieReviewRepository
    {
        private readonly ConnectionString _connectionString;

        public MovieReviewRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public void Add(int movieId,Review review)
        {
            const string sql = @"REVIEW_INSERT";
            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql,
                               new
                               {
                                   MovieId = movieId,
                                   review.Comment
                               },
                               commandType: CommandType.StoredProcedure);
        }

        public void Delete(int id)
        {
            const string sql = @"
DELETE MR
FROM MovieReviewMapping MR
WHERE MR.ReviewId = @id

DELETE Review
FROM Review
WHERE Id = @id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new { id });
        }

        public IEnumerable<Review> Get(int movieId)
        {
            const string query = @"
SELECT R.*
FROM Review R
INNER JOIN MovieReviewMapping MR ON R.Id = MR.ReviewId
WHERE MR.MovieId = @movieId";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var reviews = connection.Query<Review>(query, new { movieId });

            return reviews;
        }

        public void Update(int id,Review review)
        {
            const string sql = @"
UPDATE Review
SET Comment = @comment
WHERE Id = @id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql,
                               new 
                               {
                                   Id = id,
                                   comment = review.Comment 
                               });
        }
    }
}
