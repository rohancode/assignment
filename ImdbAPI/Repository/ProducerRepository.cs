﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using ImdbAPI.Models;
using Microsoft.Extensions.Options;

namespace ImdbAPI.Repository
{
    public class ProducerRepository : IProducerRepository
    {
        private readonly ConnectionString _connectionString;

        public ProducerRepository(IOptions<ConnectionString> connectionString)
        {
            _connectionString = connectionString.Value;
        }

        public int Add(Producer producer)
        {
            const string sql = @"
INSERT INTO Producers
VALUES (
	@Name
	,@Gender
	,@DOB
	,@BIO
	)
SELECT SCOPE_IDENTITY()";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var id = connection.Query<int>(sql, new
            {
                producer.Name,
                producer.Gender,
                DOB = producer.DOB.ToString("yyyy/MM/dd"),
                producer.Bio
            });

            return id.Single();
        }

        public void Delete(int id)
        {
            const string sql = @"DELETE_PRODUCER";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, 
                               new { ID = id }, 
                               commandType: CommandType.StoredProcedure);
        }

        public IEnumerable<Producer> GetAll()
        {
            const string query = @"
SELECT ID
	,NAME
	,GENDER
	,DOB
	,BIO
FROM Producers";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var producers = connection.Query<Producer>(query);
            return producers;
        }

        public Producer GetById(int id)
        {
            const string query = @"
SELECT Id
	,NAME
	,GENDER
	,DOB
	,BIO
FROM Producers
WHERE Id = @Id";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            var producer = connection.QueryFirst<Producer>(query, new { Id = id });
            return producer;
        }

        public void Update(int id, Producer producer)
        {
            const string sql = @"
UPDATE Producers
SET NAME = @NAME
	,DOB = @DOB
	,BIO = @BIO
	,GENDER = @GENDER
WHERE ID = @ID";

            using var connection = new SqlConnection(_connectionString.IMDBDB);
            connection.Execute(sql, new
            {
                ID = id,
                producer.Name,
                DOB = producer.DOB.ToString("yyyy/MM/dd"),
                producer.Bio,
                producer.Gender
            });
        }
    }
}
