USE [Imdb]
GO

/****** Object:  StoredProcedure [dbo].[INSERT_MOVIE]    Script Date: 26-02-2021 14:57:43 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

--SP for insert movie
CREATE PROCEDURE [dbo].[INSERT_MOVIE] 
@Name VARCHAR(100),
@YearOfRelease DATE,
@Plot VARCHAR(150),
@Poster VARCHAR(500),
@ProducerId INT,
@ActorsId VARCHAR(MAX),
@GenresId VARCHAR(MAX)
AS
INSERT INTO Movies
VALUES (@Name,@YearOfRelease,@Plot,@ProducerId,@Poster);

DECLARE @MovieId AS INT = SCOPE_IDENTITY()

INSERT INTO MoviesActorMapping 
SELECT @MovieId [MovieId]
, [value] [ActorId]
FROM string_split(@ActorsId,',')

INSERT INTO GenresMoviesMapping(MovieId,GenresId)
SELECT @MovieId [MovieId]
, [value] [GenresId]
FROM string_split(@GenresId,',')
GO


--SP for delete producer
CREATE PROCEDURE [dbo].[DELETE_PRODUCER] @Id INT
AS
DELETE MoviesActorMapping
FROM MoviesActorMapping MA
INNER JOIN Movies M
ON M.Id = MA.MovieId
WHERE M.ProducerId = @Id

DELETE GenresMoviesMapping
FROM GenresMoviesMapping GM
INNER JOIN Movies M
ON M.Id = GM.MovieId
WHERE M.ProducerId = @Id

DELETE FROM Movies
WHERE Movies.ProducerId = @Id

DELETE FROM Producers
WHERE Producers.Id = @Id
GO


--SP for update producer
CREATE PROCEDURE [dbo].[MOVIE_UPDATE]
@Name VARCHAR(100),
@YearOfRelease DATE,
@Plot VARCHAR(150),
@Poster VARCHAR(500),
@ProducerId INT,
@ActorsId VARCHAR(MAX),
@GenresId VARCHAR(MAX),
@Id INT
AS
UPDATE Movies
SET Name = @Name,
	YearofRelease=@YearOfRelease,
	Plot = @Plot,
	ProducerId = @ProducerId,
	Poster = @Poster
WHERE Id= @Id 

DELETE FROM MoviesActorMapping
WHERE MovieId=@Id

INSERT INTO MoviesActorMapping 
SELECT @Id [MovieId]
, [value] [ActorId]
FROM string_split(@ActorsId,',')


DELETE FROM GenresMoviesMapping
WHERE MovieId = @Id

INSERT INTO GenresMoviesMapping(MovieId,GenresId)
SELECT @Id [MovieId]
, [value] [GenresId]
FROM string_split(@GenresId,',')
GO
