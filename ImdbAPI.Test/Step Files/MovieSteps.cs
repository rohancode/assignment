﻿using ImdbAPI.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;


namespace ImdbAPI.Test.StepFiles
{
    [Scope(Feature = "Movie Resource")]
    [Binding]
    public class MovieSteps : BaseSteps
    {
        public MovieSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => MovieMock.MovieRepoMock.Object);
                    services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                    services.AddScoped(service => GenreMock.GenreRepoMock.Object);
                    services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                });
            }
            ))
        {

        }

        [BeforeScenario]
        public static void Mocks()
        {
            MovieMock.MockGetAll();
            MovieMock.MockGetById();
            MovieMock.MockPost();
            MovieMock.MockPut();
            MovieMock.MockDelete();
            ActorMock.MockGetByMovieId();
            GenreMock.MockGetByMovieId();
            ProducerMock.MockGetById();

            
        }

    }
}
