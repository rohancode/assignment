﻿using ImdbAPI.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;


namespace ImdbAPI.Test.StepFiles
{
    [Scope(Feature = "Genre Resource")]
    [Binding]
    public class GenreSteps : BaseSteps
    {
        public GenreSteps(CustomWebApplicationFactory<TestStartup>factory):
            base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(service =>
                {
                    service.AddScoped(service => GenreMock.GenreRepoMock.Object);
                });
            }
            ))
        {

        }

        [BeforeScenario]
        public static void Mocks()
        {
            GenreMock.MockGetAll();
            GenreMock.MockGetById();
            GenreMock.MockPost();
            GenreMock.MockPut();
            GenreMock.MockDelete();
        }

    }
}
