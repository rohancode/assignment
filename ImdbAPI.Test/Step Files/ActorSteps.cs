﻿using ImdbAPI.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;
using ImdbAPI.Services.Interface;
using ImdbAPI.Services;


namespace ImdbAPI.Test.StepFiles
{
    [Scope(Feature = "Actor Resource")]
    [Binding]
    public class ActorSteps : BaseSteps
    {
        public ActorSteps(CustomWebApplicationFactory<TestStartup> factory)
            : base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
               {
                   services.AddScoped(service => ActorMock.ActorRepoMock.Object);
                   services.AddScoped<IActorService, ActorService>();
               });
            }))
        {

        }

        [BeforeScenario]
        public static void Mocks()
        {
            ActorMock.MockGetAll();
            ActorMock.MockGetById();
            ActorMock.MockPost();
            ActorMock.MockPut();
            ActorMock.MockDelete();
        }
    }
}
