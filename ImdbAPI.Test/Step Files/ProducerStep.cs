﻿using ImdbAPI.Test.MockResources;
using Microsoft.Extensions.DependencyInjection;
using TechTalk.SpecFlow;

namespace ImdbAPI.Test.StepFiles
{
    [Scope(Feature = "Producer Resource")]
    [Binding]
    public class ProducerStep : BaseSteps
    {
        public ProducerStep(CustomWebApplicationFactory<TestStartup> factory):
            base(factory.WithWebHostBuilder(builder =>
            {
                builder.ConfigureServices(services =>
                {
                    services.AddScoped(service => ProducerMock.ProducerRepoMock.Object);
                    
                });
            }
            ))
        {

        }

        [BeforeScenario]
        public void Mocks()
        {
            ProducerMock.MockGetAll();
            ProducerMock.MockGetById();
            ProducerMock.MockPost();
            ProducerMock.MockPut();
        }
    }
}
