﻿Feature: Genre Resource


Scenario: Get all the genres
	Given I am a client
	When I make GET Request '/genres'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Action"},{"id":2,"name":"Romantic"}]'

Scenario: Get the genres by Id
	Given I am a client
	When I make GET Request '/genres/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Action"}'


Scenario: Add genre
	Given I am a client
	When I am making a post request to '/genres' with the following Data '{"Name":"romantic"}'
	Then response code must be '200'


Scenario: Update a genre
	Given I am a client
	When I make PUT Request '/genres/1' with the following Data with the following Data '{"Name":"action"}'
	Then response code must be '200'


Scenario: Delete a genre
	Given I am a client
	When I make Delete Request '/genres/1'
	Then response code must be '200'