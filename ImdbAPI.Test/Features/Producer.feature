﻿Feature: Producer Resource


Scenario: Get all the producers
	Given I am a client
	When I make GET Request '/producers'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Producer 1","bio":"--","dob":"25-08-1999","gender":"Female"}]'

Scenario: Get producer by Id
	Given I am a client
	When I make GET Request '/producers/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Producer 1","bio":"--","dob":"25-08-1999","gender":"Female"}'



Scenario: Add producer
	Given I am a client
	When I am making a post request to '/producers' with the following Data '{"Name":"producer 1","DOB":"1999/08/25","Bio":"--","Gender":"Male"}'
	Then response code must be '200'


Scenario: Update a producer
	Given I am a client
	When I make PUT Request '/producers/1' with the following Data with the following Data '{"Name":"producer 1","DOB":"1999/08/25","Bio":"--","Gender":"Female"}'
	Then response code must be '200'


Scenario: Delete a producer
	Given I am a client
	When I make Delete Request '/producers/1'
	Then response code must be '200'