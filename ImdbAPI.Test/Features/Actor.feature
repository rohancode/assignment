﻿Feature: Actor Resource


Scenario: Get all the actors
	Given I am a client
	When I make GET Request '/actors'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"Actor 1","bio":"--","dob":"25-08-1999","gender":"Male"}]'

	
Scenario: Get by the id of actor
	Given I am a client
	When I make GET Request '/actors/1/'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"Actor 1","bio":"--","dob":"25-08-1999","gender":"Male"}'



Scenario: Add actor
	Given I am a client
	When I am making a post request to '/actors' with the following Data '{"Name":"actor 1","DOB":"1999/08/25","Bio":"--","Gender":"Male"}'
	Then response code must be '200'


Scenario: Update a actor
	Given I am a client
	When I make PUT Request '/actors/1' with the following Data with the following Data '{"Name":"actor 1","Bio":"--","DOB":"1999/08/25","Gender":"Female"}'
	Then response code must be '200'


Scenario: Delete a actor
	Given I am a client
	When I make Delete Request '/actors/1'
	Then response code must be '200'