﻿Feature: Movie Resource


Scenario: Get all the movies
	Given I am a client
	When I make GET Request '/movies'
	Then response code must be '200'
	And response data must look like '[{"id":1,"name":"new","yearOfRelease":"25-08-1999","plot":"plot","poster":"basic url","actors":[{"id":1,"name":"Actor 1","bio":"--","dob":"25-08-1999","gender":"Male"}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"Romantic"}],"producer":{"id":1,"name":"Producer 1","bio":"--","dob":"25-08-1999","gender":"Female"}}]'

Scenario: Get the movie by Id
	Given I am a client
	When I make GET Request '/movies/1'
	Then response code must be '200'
	And response data must look like '{"id":1,"name":"new","yearOfRelease":"25-08-1999","plot":"plot","poster":"basic url","actors":[{"id":1,"name":"Actor 1","bio":"--","dob":"25-08-1999","gender":"Male"}],"genres":[{"id":1,"name":"Action"},{"id":2,"name":"Romantic"}],"producer":{"id":1,"name":"Producer 1","bio":"--","dob":"25-08-1999","gender":"Female"}}'

Scenario: Add movie
	Given I am a client
	When I am making a post request to '/movies' with the following Data '{"Name":"checking","YearOfRelease":"1985/10/25","Plot":"Wowo","Poster":"some random url","ActorsId":[1],"GenresId":[1,2],"ProducerId":1}'
	Then response code must be '200'


Scenario: Update a movie
	Given I am a client
	When I make PUT Request '/movies/1' with the following Data with the following Data '{"Name":"checking","YearOfRelease":"1985/10/25","Plot":"Wowo","Poster":"some random url","ActorsId":[1],"GenresId":[1,2],"ProducerId":1}'
	Then response code must be '200'


Scenario: Delete a movie
	Given I am a client
	When I make Delete Request '/movies/1'
	Then response code must be '200'