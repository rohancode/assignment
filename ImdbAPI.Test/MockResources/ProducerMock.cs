﻿using System;
using System.Collections.Generic;
using Moq;
using ImdbAPI.Models;
using ImdbAPI.Repository;
using System.Linq;


namespace ImdbAPI.Test.MockResources
{
    public class ProducerMock
    {
        public static readonly Mock<IProducerRepository> ProducerRepoMock = new Mock<IProducerRepository>();

        public static void MockGetAll()
        {
            ProducerRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfProducers());
        }

        public static void MockGetById()
        {
            ProducerRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
                                .Returns((int id) => ListOfProducers().Single(obj => obj.Id == id));
        }

        public static void MockPost()
        {
            ProducerRepoMock.Setup(repo => repo.Add(It.IsAny<Producer>()));
        }

        public static void MockPut()
        {
            ProducerRepoMock.Setup(repo => repo.Update(It.IsAny<int>(),
                                                    It.IsAny<Producer>()));
        }

        public static void MockDelete()
        {
            ProducerRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        private static IEnumerable<Producer> ListOfProducers()
        {
            return new List<Producer>
            {
                new Producer
                {
                    Id = 1,
                    Name = "Producer 1",
                    Bio = "--",
                    DOB = DateTime.ParseExact("1999/08/25","yyyy/MM/dd",null),
                    Gender="Female"
                }
            };
        }
    }
}
