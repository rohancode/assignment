﻿using System.Collections.Generic;
using Moq;
using ImdbAPI.Models;
using ImdbAPI.Repository;
using System.Linq;

namespace ImdbAPI.Test.MockResources
{
    public class GenreMock
    {
        public static readonly Mock<IGenreRepository> GenreRepoMock = new Mock<IGenreRepository>();

        public static void MockGetAll()
        {
            GenreRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfGenres());
        }

        public static void MockGetById()
        {
            GenreRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
                            .Returns((int id) => ListOfGenres().Single(obj => obj.Id == id));
        }

        public static void MockGetByMovieId()
        {

            GenreRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>()))
                                    .Returns((int id) =>
                                    {
                                        var genresId = MovieGenreMapping(id);
                                        return ListOfGenres().Where(obj => genresId.Contains(obj.Id));
                                    });
        }

        public static void MockPost()
        {
            GenreRepoMock.Setup(repo => repo.Add(It.IsAny<Genre>()));
        }

        public static void MockPut()
        {
            GenreRepoMock.Setup(repo => repo.Update(It.IsAny<int>(),
                                                    It.IsAny<Genre>()));
        }

        public static void MockDelete()
        {
            GenreRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        private static IEnumerable<int> MovieGenreMapping(int id)
        {
            var mapping = new Dictionary<int, List<int>>();
            var listOfGenresId = new List<int>() { 1,2 };
            mapping.Add(1, listOfGenresId);
            return mapping[id];
        }

        private static IEnumerable<Genre> ListOfGenres()
        {
            return new List<Genre>
            {
                new Genre
                {
                    Id=1,
                    Name="Action"
                },
                new Genre
                {
                    Id=2,
                    Name="Romantic"
                }
            };

        }
    }
}
