﻿using System;
using System.Collections.Generic;
using Moq;
using ImdbAPI.Models;
using ImdbAPI.Repository;
using System.Linq;

namespace ImdbAPI.Test.MockResources
{
    public class ActorMock
    {
        public static readonly Mock<IActorRepository> ActorRepoMock = new Mock<IActorRepository>();
        
        public static void MockGetAll()
        {
            ActorRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfActors());
        }

        public static void MockGetById()
        {
            ActorRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
                            .Returns((int id) => ListOfActors().Single(a => a.Id == id));
        }

        public static void MockGetByMovieId()
        {
            ActorRepoMock.Setup(repo => repo.GetByMovieId(It.IsAny<int>()))
                                    .Returns((int id) =>
                                    {
                                        var actorsId = MovieActorMapping(id);
                                        return ListOfActors().Where(obj => actorsId.Contains(obj.Id));
                                    });
        }

        public static void MockPost()
        {
            ActorRepoMock.Setup(repo => repo.Add(It.IsAny<Actor>()));
        }

        public static void MockPut()
        {
            ActorRepoMock.Setup(repo => repo.Update(It.IsAny<int>(),
                                                    It.IsAny<Actor>()));
        }

        public static void MockDelete()
        {
            ActorRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        private static IEnumerable<int> MovieActorMapping(int id)
        {
            var mapping = new Dictionary<int, List<int>>();
            var listOfActorsId = new List<int>() { 1 };
            mapping.Add(1, listOfActorsId);
            return mapping[id];
        }

        private static IEnumerable<Actor> ListOfActors()
        {
            List<Actor> _actorDb = new List<Actor>();
             var actor = new Actor
            {
                Id = 1,
                Name = "Actor 1",
                Bio = "--",
                Gender = "Male",
                DOB = DateTime.ParseExact("1999/08/25", "yyyy/MM/dd", null)
            };
            _actorDb.Add(actor);
            return _actorDb;
        }



    }
}
