﻿using System;
using System.Collections.Generic;
using Moq;
using ImdbAPI.Models;
using ImdbAPI.Repository.Interface;
using System.Linq;

namespace ImdbAPI.Test.MockResources
{
    public class MovieMock
    {
        public static readonly Mock<IMovieRepository> MovieRepoMock = new Mock<IMovieRepository>();

        public static void MockGetAll()
        {
            MovieRepoMock.Setup(repo => repo.GetAll()).Returns(ListOfMovies());
        }
        public static void MockGetById()
        {
            MovieRepoMock.Setup(repo => repo.GetById(It.IsAny<int>()))
                                        .Returns((int id) => ListOfMovies().Single(obj => obj.Id == id));
        }

        public static void MockPost()
        {
            MovieRepoMock.Setup(repo => repo.Add(It.IsAny<Movie>(),
                                                 It.IsAny<List<int>>(),
                                                 It.IsAny<List<int>>()));                     
        }

        public static void MockPut()
        {
            MovieRepoMock.Setup(repo => repo.Update(It.IsAny<int>(),
                                                    It.IsAny<Movie>(),
                                                    It.IsAny<List<int>>(),
                                                    It.IsAny<List<int>>()));
        }

        public static void MockDelete()
        {
            MovieRepoMock.Setup(repo => repo.Delete(It.IsAny<int>()));
        }

        private static IEnumerable<Movie> ListOfMovies()
        {
            var listOfMovie = new List<Movie>();
            var movie = new Movie()
            {
                Id = 1,
                Name = "new",
                YearOfRelease = DateTime.ParseExact("1999/08/25", "yyyy/MM/dd", null),
                Plot = "plot",
                Poster = "basic url",
                ProducerId = 1

            };
            listOfMovie.Add(movie);
            return listOfMovie;
        }
    }
}
